git reset --hard
git pull
docker-compose -f dc.production.yml kill
docker-compose -f dc.production.yml build --force-rm
docker-compose -f dc.production.yml up -d --force-recreate --remove-orphans
