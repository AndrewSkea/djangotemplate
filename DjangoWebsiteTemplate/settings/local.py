from .base import *

SECRET_KEY = '7%g$nfh#9r_#8x#8(=q@vd39_6cpt2g2nca-s+&^$g6x4qlk(@'

DEBUG = True

ALLOWED_HOSTS = ['*',]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../../db.sqlite3'),
    }
}