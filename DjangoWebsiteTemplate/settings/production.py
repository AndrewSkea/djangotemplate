from .base import *

SECRET_KEY = '7%g$nfh#9r_#8x#8(=q@vd39_6cpt2g2nca-s+&^$g6x4qlk(@'

DEBUG = False

ALLOWED_HOSTS = ['example', 'example.co.uk', 'example.com', 'www.example.co.uk', 'www.example.com']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': os.environ.get('POSTGRES_HOST'),
        'PORT': os.environ.get('POSTGRES_PORT')
    }
}