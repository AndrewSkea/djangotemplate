from django.urls import path
from .views import HomeView, CookiePolicyView

app_name = "main"

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('cookie-policy/', CookiePolicyView.as_view(), name='cookie-policy'),
]